#include "DomesticBoard.h"

#include <functional>
#include <string>
#include "../Hardware/DomesticBoardOpt.h"

bool DomesticBoard::open()
{
    Board::open();

    int comPort = 3;
    int baudRate = 1000000;//9600;
    COMMTIMEOUTS timeOut;
	timeOut.ReadIntervalTimeout = MAXDWORD;
	timeOut.ReadTotalTimeoutMultiplier = 500;
	timeOut.ReadTotalTimeoutConstant = 5000;
	timeOut.WriteTotalTimeoutMultiplier = 500;
	timeOut.WriteTotalTimeoutConstant = 2000;

    bool result = _serialPort.initPort(comPort, baudRate, 1, 8, 1, &timeOut);
    if (result)
    {
        onOpen();
    }
    else
    {
        printf("Init COM error.\n");

        _serialPort.closePort();
    }
    return result;
}

void DomesticBoard::close()
{
    Board::close();
}

void DomesticBoard::update(int delta)
{
    Board::update(delta);
}

void DomesticBoard::readDeviceInfo(InfoType infoType)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadDeviceInfoOpt(&_serialPort, infoType, std::bind(&DomesticBoard::printDeviceInfo, this, std::placeholders::_1))));
    }
}

void DomesticBoard::changeBaudrate(int baudrate)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ChangeBaudrateOpt(&_serialPort, baudrate, nullptr)));
    }
}

void DomesticBoard::comTest()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ComTestOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readStatus()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadStatusOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::clearAllError()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ClearAllErrorOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::syncTime()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new SyncTimeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readTime()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadTimeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::writeParameter(ParaType paraType)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new WriteParameterOpt(&_serialPort, paraType, nullptr)));
    }
}

void DomesticBoard::readParameter()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadParameterOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::startAcquire()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new StartAcquireOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readAcquiredData()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadAcquiredDataOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::writeRamData(int ramAddress, int data, int writeLen)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new WriteRamDataOpt(&_serialPort, ramAddress, data, writeLen, nullptr)));
    }
}

void DomesticBoard::readRamData(int ramAddress, int readLen)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadRamDataOpt(&_serialPort, ramAddress, readLen, nullptr)));
    }
}

void DomesticBoard::enterDVPMode()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new EnterDVPModeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::exitDVPMode()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ExitDVPModeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::terminateExe()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new TerminateExeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readAuxInfo()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadAuxInfoOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::enterBSLMode()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new EnterBSLModeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::printDeviceInfo(std::vector<std::string> vector)
{
    for (int i = 0; i < vector.size(); i++)
    {
        std::cout << vector[i] << std::endl;
    }
}

void DomesticBoard::onOpen()
{

}

void DomesticBoard::onClose()
{

}