#include "DomesticBoardOpt.h"

#include <iostream>
#include <random>
#include <ctime>
#include <assert.h>

ReadDeviceInfoOpt::ReadDeviceInfoOpt(SerialPortImpl* comHandle, InfoType infoType, CallbackStringVector onReply)
    : SerialPortOpt(1, comHandle)
    , _infoType(infoType)
    , _replyEvent(onReply)
{

}

void ReadDeviceInfoOpt::onExecute()
{
    char instruction = INSB_READ_DEVICE_INFO;
    unsigned short data = _infoType;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = data;

    sendInstruction();
}

void ReadDeviceInfoOpt::onReply()
{
    if (_errorCode == 0)
    {
        if (_replyEvent)
        {
            _replyEvent(_vector);
        }
    }
    else
    {
        if (_errorCode == 0x8000)
        {
            // Info Type不存在
            // TODO
        }
    }
}

bool ReadDeviceInfoOpt::parseResponse()
{
    _vector.clear();

    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;

    switch (_infoType)
    {
    case DeviceType:
    {
        unsigned short deviceTypeLength = *(buffer++);
        _vector.push_back(extractString(buffer, deviceTypeLength));
    }
    break;
    case HardwareVersion:
    {
        unsigned short comBoardVersionLength = *(buffer++);
        _vector.push_back(extractString(buffer, comBoardVersionLength));

        buffer += comBoardVersionLength;
        unsigned short motherBoardVersionLength = *(buffer++);
        _vector.push_back(extractString(buffer, motherBoardVersionLength));

        buffer += motherBoardVersionLength;
        unsigned short coreBoardVersionLength = *(buffer++);
        _vector.push_back(extractString(buffer, coreBoardVersionLength));
    }
    break;
    case SoftwareVersion:
    {
        unsigned short comBoardVersionLength = *(buffer++);
        _vector.push_back(extractString(buffer, comBoardVersionLength));

        buffer += comBoardVersionLength;
        unsigned short coreBoardVersionLength = *(buffer++);
        _vector.push_back(extractString(buffer, coreBoardVersionLength));
    }
    break;
    case RAMVersion:
    {
        unsigned short ramVersionLength = *(buffer++);
        unsigned short ramType = *(buffer++);
        unsigned short storageDepth1 = *(buffer++);
        unsigned short storageDepth2 = *(buffer++);
        unsigned short bitWdith = *(buffer++);
        _vector.push_back(extractString(buffer, ramVersionLength - 4));
    }
    break;
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////

ChangeBaudrateOpt::ChangeBaudrateOpt(SerialPortImpl* comHandle, int baudrate, CallbackVoid onReply)
    : SerialPortOpt(2, comHandle)
    , _baudrate(baudrate)
    , _replyEvent(onReply)
{

}

void ChangeBaudrateOpt::onExecute()
{
    std::cout << "ChangeBaudrateOpt" << std::endl;

    char instruction = INSB_CHANGE_BAUDRATE;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0;
    _instructionBuffer[3] = _baudrate / 100;

    sendInstruction();
}

void ChangeBaudrateOpt::onReply()
{
    std::cout << "ChangeBaudrateOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ChangeBaudrateOpt::parseResponse()
{
    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    unsigned short changeFlag = *(buffer++);        // 1.设备接收并改变波特率, 2.设备不改变波特率
    buffer++;
    unsigned short receivedBaudrate = *(buffer++);  // 接收到的波特率
    buffer++;
    unsigned short newBaudrate = *(buffer++);       // 设备新的波特率

    return true;
}

//////////////////////////////////////////////////////////////////////////

ComTestOpt::ComTestOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _replyEvent(onReply)
{

}

void ComTestOpt::onExecute()
{
    std::cout << "ComTestOpt" << std::endl;

    char instruction = INSB_COM_TEST;

    // 产生随机数作为数据长度(50-400)
    std::default_random_engine e(static_cast<unsigned int>(time(nullptr)));
    std::uniform_int_distribution<int> distribInt(50, 400);
    _sendDataLen = distribInt(e);

    // 重新分配内存
    allocateBuffer();

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    // 随机填充
    std::uniform_int_distribution<unsigned short> distribShortInt(0, USHRT_MAX);
    for (int i = 0; i < _sendDataLen; i++)
    {
        _instructionBuffer[i + INSTRUCTION_DATA_OFFSET] = distribShortInt(e);
    }

    sendInstruction();
}

void ComTestOpt::onReply()
{
    std::cout << "ComTestOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ComTestOpt::parseResponse()
{
    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    // Print received random data
    for (int i = 0; i < _responseDataLen - RESPONSE_DATA_OFFSET - 1; i++)
    {
        printf("%X", *(buffer++));
    }
    printf("\n");

    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadStatusOpt::ReadStatusOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _statusWordHigh(0)
    , _statusWordLow(0)
    , _replyEvent(onReply)
{

}

void ReadStatusOpt::onExecute()
{
    std::cout << "ReadStatusOpt" << std::endl;

    char instruction = INSB_READ_STATUS;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendInstruction();
}

void ReadStatusOpt::onReply()
{
    std::cout << "ReadStatusOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadStatusOpt::parseResponse()
{
    _statusWordHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    _statusWordLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    int topLoopNum = _statusWordHigh;
    unsigned short FPGAState = _statusWordLow & 0x000F;
    unsigned short FPGACfg = (_statusWordLow & 0x0010) >> 4;
    unsigned short FPGAPLL = (_statusWordLow & 0x0020) >> 5;

    return true;
}

//////////////////////////////////////////////////////////////////////////

ClearAllErrorOpt::ClearAllErrorOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void ClearAllErrorOpt::onExecute()
{
    std::cout << "ClearAllErrorOpt" << std::endl;

    char instruction = INSB_CLEAR_ALL_ERROR;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0505;

    sendInstruction();
}

void ClearAllErrorOpt::onReply()
{
    std::cout << "ClearAllErrorOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

//////////////////////////////////////////////////////////////////////////

SyncTimeOpt::SyncTimeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(2, comHandle)
    , _replyEvent(onReply)
{

}

void SyncTimeOpt::onExecute()
{
    std::cout << "SyncTimeOpt" << std::endl;

    char instruction = INSB_SYNC_TIME;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0;  // Timestamp high word
    _instructionBuffer[3] = 0;  // Timestamp low word

    sendInstruction();
}

void SyncTimeOpt::onReply()
{
    std::cout << "SyncTimeOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

//////////////////////////////////////////////////////////////////////////

ReadTimeOpt::ReadTimeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _timeStampHigh(0)
    , _timeStampLow(0)
    , _replyEvent(onReply)
{

}

void ReadTimeOpt::onExecute()
{
    std::cout << "ReadTimeOpt" << std::endl;

    char instruction = INSB_READ_TIME;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendInstruction();
}

void ReadTimeOpt::onReply()
{
    std::cout << "ReadTimeOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadTimeOpt::parseResponse()
{
    _timeStampHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    _timeStampLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    return true;
}

//////////////////////////////////////////////////////////////////////////

WriteParameterOpt::WriteParameterOpt(SerialPortImpl* comHandle, ParaType paraType, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _paraType(paraType)
    , _replyEvent(onReply)
{

}

void WriteParameterOpt::onExecute()
{
    std::cout << "WriteParameterOpt" << std::endl;

    char instruction = INSB_WRITE_PARA;

    if (_paraType == 1)
    {
        // 64个FIR滤波器参数
        _sendDataLen = 64;
    }
    else if (_paraType == 2)
    {
        // 1个CIC参数
        _sendDataLen = 1;
    }
    else if (_paraType == 3)
    {
        // 时间核代码,长度为4的倍数
    }

    // 重新分配内存
    allocateBuffer();

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    if (_paraType == 1)
    {
        // 64个FIR滤波器参数        
    }
    else if (_paraType == 2)
    {
        // 1个CIC参数
        _instructionBuffer[2] = 0;
    }
    else if (_paraType == 3)
    {
        // 时间核代码,长度为4的倍数
    }

    sendInstruction();
}

void WriteParameterOpt::onReply()
{
    std::cout << "WriteParameterOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool WriteParameterOpt::parseResponse()
{


    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadParameterOpt::ReadParameterOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _replyEvent(onReply)
{

}

void ReadParameterOpt::onExecute()
{
    std::cout << "ReadParameterOpt" << std::endl;

    char instruction = INSB_READ_PARA;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendInstruction();
}

void ReadParameterOpt::onReply()
{
    std::cout << "ReadParameterOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadParameterOpt::parseResponse()
{
    // 解析读取到的参数

    return true;
}

//////////////////////////////////////////////////////////////////////////

StartAcquireOpt::StartAcquireOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void StartAcquireOpt::onExecute()
{
    std::cout << "StartAcquireOpt" << std::endl;

    char instruction = INSB_START_ACQUIRE;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0A0A;

    sendInstruction();
}

void StartAcquireOpt::onReply()
{
    std::cout << "StartAcquireOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool StartAcquireOpt::parseResponse()
{
    if (_errorCode == 0x0200)
    {
        // 指令执行失败
        // TODO
    }
    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadAcquiredDataOpt::ReadAcquiredDataOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _replyEvent(onReply)
{

}

void ReadAcquiredDataOpt::onExecute()
{
    std::cout << "ReadAcquiredDataOpt" << std::endl;

    char instruction = INSB_READ_ACQUIRED_DATA;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendInstruction();
}

void ReadAcquiredDataOpt::onReply()
{
    std::cout << "ReadAcquiredDataOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadAcquiredDataOpt::parseResponse()
{
    return true;
}

//////////////////////////////////////////////////////////////////////////

WriteRamDataOpt::WriteRamDataOpt(SerialPortImpl* comHandle, int ramAddress, int data, int length, CallbackVoid onReply)
    : SerialPortOpt(0, comHandle)
    , _ramAddress(ramAddress)
    , _data(data)
    , _length(length)
    , _replyEvent(onReply)
{

}

void WriteRamDataOpt::onExecute()
{
    std::cout << "WriteRAMDataOpt" << std::endl;

    char instruction = INSB_WRITE_RAW_DATA;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = _ramAddress;

    sendInstruction();
}

void WriteRamDataOpt::onReply()
{
    std::cout << "WriteRAMDataOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool WriteRamDataOpt::parseResponse()
{
    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadRamDataOpt::ReadRamDataOpt(SerialPortImpl* comHandle, int ramAddress, int length, CallbackVoid onReply)
    : SerialPortOpt(5, comHandle)
    , _ramAddress(ramAddress)
    , _length(length)
    , _replyEvent(onReply)
{

}

void ReadRamDataOpt::onExecute()
{
    std::cout << "ReadRAMDataOpt" << std::endl;

    char instruction = INSB_READ_RAW_DATA;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendInstruction();
}

void ReadRamDataOpt::onReply()
{
    std::cout << "ReadRAMDataOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadRamDataOpt::parseResponse()
{
    return true;
}

//////////////////////////////////////////////////////////////////////////

EnterDVPModeOpt::EnterDVPModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void EnterDVPModeOpt::onExecute()
{
    std::cout << "EnterDVPModeOpt" << std::endl;

    char instruction = INSB_ENTER_DVP_MODE;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0E0E;

    sendInstruction();
}

void EnterDVPModeOpt::onReply()
{
    std::cout << "EnterDVPModeOpt onReply" << std::endl;

    if (_errorCode == 0x8000)
    {
        // 收到的data1数据错误
    }
    else if (_errorCode == 0x0100)
    {
        // 进入开发模式失败
    }

    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool EnterDVPModeOpt::parseResponse()
{
    return true;
}

//////////////////////////////////////////////////////////////////////////

ExitDVPModeOpt::ExitDVPModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void ExitDVPModeOpt::onExecute()
{
    std::cout << "ExitDVPModeOpt" << std::endl;

    char instruction = INSB_EXIT_DVP_MODE;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0F0F;

    sendInstruction();
}

void ExitDVPModeOpt::onReply()
{
    std::cout << "ExitDVPModeOpt onReply" << std::endl;

    if (_errorCode == 0x8000)
    {
        // 退出开发模式失败
    }

    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ExitDVPModeOpt::parseResponse()
{
    return true;
}

//////////////////////////////////////////////////////////////////////////

TerminateExeOpt::TerminateExeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void TerminateExeOpt::onExecute()
{
    std::cout << "TerminateExeOpt" << std::endl;

    char instruction = INSB_TEMINATE_EXE;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0101;

    sendInstruction();
}

void TerminateExeOpt::onReply()
{
    std::cout << "TerminateExeOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool TerminateExeOpt::parseResponse()
{
    unsigned short statusWordHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    unsigned short statusWordLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    int topLoopNum = statusWordHigh;
    unsigned short FPGAState = statusWordLow & 0x000F;
    unsigned short FPGACfg = (statusWordLow & 0x0010) >> 4;
    unsigned short FPGAPLL = (statusWordLow & 0x0020) >> 5;

    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadAuxInfoOpt::ReadAuxInfoOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void ReadAuxInfoOpt::onExecute()
{
    std::cout << "ReadAuxInfoOpt" << std::endl;

    char instruction = INSB_READ_AUX_INFO;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 1;      // 读取单片机温度

    sendInstruction();
}

void ReadAuxInfoOpt::onReply()
{
    std::cout << "ReadAuxInfoOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadAuxInfoOpt::parseResponse()
{
    unsigned short auxInfo = _responseBuffer[RESPONSE_DATA_OFFSET];

    return true;
}

//////////////////////////////////////////////////////////////////////////

EnterBSLModeOpt::EnterBSLModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(1, comHandle)
    , _replyEvent(onReply)
{

}

void EnterBSLModeOpt::onExecute()
{
    std::cout << "EnterBSLModeOpt" << std::endl;

    char instruction = INSB_ENTER_BSL_MODE;

    // 装配指令
    _instructionBuffer[0] = (instruction << 8) | (~instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x2828;

    sendInstruction();
}

void EnterBSLModeOpt::onReply()
{
    std::cout << "EnterBSLModeOpt onReply" << std::endl;
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool EnterBSLModeOpt::parseResponse()
{
    unsigned short auxInfo = _responseBuffer[RESPONSE_DATA_OFFSET];

    return true;
}