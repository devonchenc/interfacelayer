#include "Board.h"

#include "../Hardware/DomesticBoard.h"

BoardStation& BoardStation::instance()
{
    static BoardStation instance;
    return instance;
}

void BoardStation::update(int delta)
{
    if (_boardPtr)
    {
        _boardPtr->update(delta);
    }
}

bool BoardStation::open()
{
    bool result = false;

    int deviceType = 0;
    switch (deviceType)
    {
    case 0:
        _boardPtr = BoardPtr(new DomesticBoard());
        break;
    default:
        assert(!"never arrived here!");
    }

    if (_boardPtr)
    {
        result = _boardPtr->open();
    }

    return result;
}

void BoardStation::close()
{
    if (_boardPtr)
    {
        _boardPtr->close();
        _boardPtr = nullptr;
    }
}

bool BoardStation::isOpened() const
{
    return (_boardPtr != nullptr) && _boardPtr->isOpened();
}
