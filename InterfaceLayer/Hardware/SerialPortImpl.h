﻿#pragma once

#include <Windows.h>

#define UsingCriticalSection 0

/** 串口通信类
 *   
 *  本类实现了对串口的基本操作
 *  例如监听发到指定串口的数据、发送指定数据到串口
 */
class SerialPortImpl
{
public:
	SerialPortImpl();
	~SerialPortImpl();

public:
	/** 初始化串口函数
	 *
	 *  @param:  UINT portNo 串口编号,默认值为1,即COM1,注意,尽量不要大于9
	 *  @param:  UINT baud   波特率,默认为9600
	 *  @param:  char parity 是否进行奇偶校验,0表示无校验,1为奇校验,2为偶校验,3为标记校验
	 *  @param:  UINT databits 数据位的个数,默认值为8个数据位
	 *  @param:  UINT stopsbits 停止位使用格式,默认值为1
	 *  @return: bool  初始化是否成功
	 *  @note:   在使用其他本类提供的函数前,请先调用本函数进行串口的初始化
	 *　　　　　 \n本函数提供了一些常用的串口参数设置,若需要自行设置详细的DCB参数,可使用重载函数
	 *           \n本串口类析构时会自动关闭串口,无需额外执行关闭串口
	 *  @see:    
	 */
	bool initPort(UINT portNo = 1, UINT baud = CBR_9600, char parity = 0, UINT databits = 8, 
		           UINT stopsbits = 1, COMMTIMEOUTS* commTimeouts = nullptr);

	/** 获取串口缓冲区中的字节数
	 *
	 *  
	 *  @return: UINT  操作是否成功
	 *  @note:   当串口缓冲区中无数据时,返回0
	 *  @see:    
	 */
	UINT getBytesInCOM();

	/** 读取串口接收缓冲区中一个字节的数据
	 *
	 *  
	 *  @param:  char & cRecved 存放读取数据的字符变量
	 *  @return: bool  读取是否成功
	 *  @note:   
	 *  @see:    
	 */
	bool readChar(char& cRecved);

	bool readData(char* pData, unsigned int length, DWORD &BytesRead);

    /** 向串口写数据
     *
     *  将缓冲区中的数据写入到串口
     *  @param:  unsigned char* pData 指向需要写入串口的数据缓冲区
     *  @param:  unsigned int length 需要写入的数据长度
     *  @return: bool  操作是否成功
     *  @note:   length不要大于pData所指向缓冲区的大小
     */
    bool writeData(const char* pData, unsigned int length);

    // 写数据后接收数据
    bool callSerial(const char* inBuffer, unsigned int byteLength,
        char* outBuffer, unsigned int outBufferSize, unsigned long& readByteCount);

    HANDLE comHandle() { return _comHandle; }

    bool isOpened() const
    {
        return _comHandle != (HANDLE)INVALID_HANDLE_VALUE;
    }

    // 关闭串口
    void closePort();

private:
	// 打开串口
	bool openPort(UINT portNo);

private:	
	// 串口句柄
	HANDLE _comHandle;

#if UsingCriticalSection
	// 同步互斥,临界区保护
	CRITICAL_SECTION   _csCommunicationSync;       // 互斥操作串口
#endif
};
