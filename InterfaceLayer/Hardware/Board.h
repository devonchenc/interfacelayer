#pragma once

#include <iostream>

#include "../Global.h"
#include "../Operations/Operation.h"
#include "../Operations/Scheduler.h"

class Board
{
public:
    Board() {}
    virtual ~Board()
    {
        _scheduler.stop();
        _scheduler.cancel();
    }

    virtual bool open()
    {
        _scheduler.start();
        return true;
    }

    virtual void close()
    {
        _scheduler.stop();
         _scheduler.cancel();
    }

    virtual bool isOpened() const { return false; }

    virtual void update(int delta)
    {
        _scheduler.update();
    }

    virtual void readDeviceInfo(InfoType infoType) {}

    virtual void changeBaudrate(int baudrate) {}

    virtual void comTest() {}

    virtual void readStatus() {}

    virtual void clearAllError() {}

    virtual void syncTime() {}

    virtual void readTime() {}

    virtual void writeParameter(ParaType paraType) {}

    virtual void readParameter() {}

    virtual void startAcquire() {}

    virtual void readAcquiredData() {}

    virtual void writeRamData(int ramAddress, int data, int writeLen) {}

    virtual void readRamData(int ramAddress, int readLen) {}

    virtual void enterDVPMode() {}

    virtual void exitDVPMode() {}

    virtual void terminateExe() {}

    virtual void readAuxInfo() {}

    virtual void enterBSLMode() {}

protected:
    virtual void onOpen() {}
    virtual void onClose() {}

protected:
    Scheduler _scheduler;

    std::string _deviceName;
};

typedef std::shared_ptr<Board> BoardPtr;

class BoardStation
{
public:
    static BoardStation& instance();
    void update(int delta);

    bool open();
    void close();

    bool isOpened() const;

    BoardPtr device() { return _boardPtr; }

private:
    BoardPtr _boardPtr;
};