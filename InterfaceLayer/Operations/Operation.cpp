#include "Operation.h"

#include <thread>
#include <chrono>

Operation::Operation()
    : _delayTime(0)
    , _isLoop(false)
    , _isCanceled(false)
{

}

void Operation::execute()
{
    onExecute();

    if (_delayTime != 0)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(_delayTime));
    }
}

void Operation::reply()
{
    if (!_isCanceled)
    {
        onReply();
    }
}