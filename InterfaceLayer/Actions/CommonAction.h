﻿#pragma once

#include "ActionBase.h"

class LogAction : public ActionBase
{
public:
    LogAction(const std::string& log)
        : _logText(log)
    {}

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }

private:
    std::string _logText;
};