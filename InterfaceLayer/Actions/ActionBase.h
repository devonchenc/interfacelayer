﻿#pragma once

#include <functional>
#include <string>

typedef std::function<void(void)> EventHandler;

enum EActionState
{
    eAS_Idle = 0,
    eAS_Run,
    eAS_Finish,
    eAS_Cancel,
    eAS_Error,
};

class ActionBase
{
    friend class ActionRunner;

public:
    ActionBase();
    virtual ~ActionBase() {}

public:
    virtual std::string name() { return ""; }

    virtual void cancel() { setState(eAS_Cancel); }

    virtual void redo() { setState(eAS_Idle); }

    void setTimeNeeded(unsigned int neededTime) { _neededTime = neededTime; }

    void update(unsigned int delta);

    void ignoreTime() { _ignoreTime = true; }

    int elapsedTime() const { return _elapsedTime; }

    bool isState(EActionState state) const { return _state == state; }

protected:
    // 执行具体操作
    virtual void onExecute() = 0;

    // 判断操作是否完成
    virtual bool onEstimate() { return false; }

    virtual int onCancle() { return 0; }

    virtual int onError() { return 0; }

    std::string errorString() { return _errorString; }

    unsigned int neededTime() { return _neededTime; }

    bool isTimeOut() const { return _elapsedTime > _neededTime; }

    void setState(EActionState state) { _state = state; }

protected:
    // 已经花费的时间(毫秒)
    unsigned int _elapsedTime;

    std::string _errorString = std::string("System unknown error.");

private:
    // 执行所需时间的上限(毫秒)
    unsigned int _neededTime;

    // 是否忽略超时异常
    bool _ignoreTime;

    // 状态
    EActionState _state;
};

typedef std::shared_ptr<ActionBase> ActionPtr;