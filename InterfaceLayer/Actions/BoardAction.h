﻿#pragma once

#include "ActionBase.h"
#include "../Global.h"

class OpenBoardAction : public ActionBase
{
public:
    OpenBoardAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("连接主板"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ReadDeviceInfoAction : public ActionBase
{
public:
    ReadDeviceInfoAction(InfoType infoType) : _infoType(infoType) { setTimeNeeded(5000); }

    std::string name() { return std::string("读取设备信息"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;

private:
    InfoType _infoType;
};

class ChangeBaudrateAction : public ActionBase
{
public:
    ChangeBaudrateAction(int baudrate) : _baudrate(baudrate) { setTimeNeeded(5000); }

    std::string name() { return std::string("改变设备波特率"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;

private:
    int _baudrate;
};

class ComTestAction : public ActionBase
{
public:
    ComTestAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("通讯测试"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ReadStatusAction : public ActionBase
{
public:
    ReadStatusAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("读取设备状态"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ClearAllErrorAction : public ActionBase
{
public:
    ClearAllErrorAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("清除设备错误状态"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class SyncTimeAction : public ActionBase
{
public:
    SyncTimeAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("同步时间"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ReadTimeAction : public ActionBase
{
public:
    ReadTimeAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("读取设备时间"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class WriteParameterAction : public ActionBase
{
public:
    WriteParameterAction(ParaType paraType) : _paraType(paraType) { setTimeNeeded(5000); }

    std::string name() { return std::string("写参数"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;

private:
    ParaType _paraType;
};

class ReadParameterAction : public ActionBase
{
public:
    ReadParameterAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("读参数"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class StartAcquireAction : public ActionBase
{
public:
    StartAcquireAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("开始采集"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ReadAcquiredDataAction : public ActionBase
{
public:
    ReadAcquiredDataAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("读取采集完成的数据"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class WriteRamDataAction : public ActionBase
{
public:
    WriteRamDataAction(int ramAddress, int data, int writeLen)
        : _ramAddress(ramAddress)
        , _data(data)
        , _writeLen(writeLen)
    {
        setTimeNeeded(5000);
    }

    std::string name() { return std::string("写数据至设备RAM"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;

private:
    int _ramAddress;
    int _data;
    int _writeLen;
};

class ReadRamDataAction : public ActionBase
{
public:
    ReadRamDataAction(int ramAddress, int readLen)
        : _ramAddress(ramAddress)
        , _readLen(readLen)
    {
        setTimeNeeded(5000);
    }

    std::string name() { return std::string("从设备RAM读取数据"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;

private:
    int _ramAddress;
    int _readLen;
};

class EnterDVPModeAction : public ActionBase
{
public:
    EnterDVPModeAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("进入开发模式"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ExitDVPModeAction : public ActionBase
{
public:
    ExitDVPModeAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("退出开发模式"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class TerminateExeAction : public ActionBase
{
public:
    TerminateExeAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("强制停止设备一切工作"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class ReadAuxInfoAction : public ActionBase
{
public:
    ReadAuxInfoAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("读取设备辅助信息"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};

class EnterBSLModeAction : public ActionBase
{
public:
    EnterBSLModeAction() { setTimeNeeded(5000); }

    std::string name() { return std::string("进入BSL状态"); }

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }
    int onError() override;
};