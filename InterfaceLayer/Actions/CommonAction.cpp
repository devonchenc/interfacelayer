﻿#include "CommonAction.h"

#include "../Dumper.h"

void LogAction::onExecute()
{
    Dumper::instance().info(_logText);
}