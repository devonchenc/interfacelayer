﻿#pragma once

#include <deque>
#include "ActionBase.h"

typedef std::function<void(int)> CancelHandler;
typedef std::function<void(int, const std::string& str)> ErrorHandler;

class ActionRunner
{
public:
    virtual ~ActionRunner();

    static ActionRunner& instance();

    void runAction(ActionPtr action, bool ignoreTime = false, bool isPrior = false);

    void update(unsigned int delta);

    bool isEmpty() const;

    int count() const;

    void cancel();

    std::string activeActionName();

    void popCancelAction() { _cancelSequence.pop_front(); }

    void popErrorAction() { _errorSequence.pop_front(); }

    void reRunCurrentAction() { _workSequence.push_front(_activeAction); }

    void setErrorHandler(ErrorHandler handler) { _errorHandler = handler; }

private:
    ActionRunner();

private:
    std::deque<ActionPtr> _workSequence;
    std::deque<ActionPtr> _cancelSequence;
    std::deque<ActionPtr> _errorSequence;

    ActionPtr _activeAction;

    int _totalActionCount;

    unsigned int _elapsedTime;

    unsigned int _remainTime;

    bool _enableStatistics;

    CancelHandler _cancelHandler;

    ErrorHandler _errorHandler;
};