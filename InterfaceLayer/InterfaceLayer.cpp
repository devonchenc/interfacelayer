﻿#include <iostream>

#include <Windows.h>

#include "Hardware/Board.h"
#include "Actions/ActionRunner.h"
#include "Actions/BoardAction.h"

#include "Global.h"

int main()
{
    ActionRunner::instance().runAction(ActionPtr(new OpenBoardAction()));
 //   ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::DeviceType)));
//    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::HardwareVersion)));
 //   ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::SoftwareVersion)));
 //   ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::RAMVersion)));
 //   ActionRunner::instance().runAction(ActionPtr(new ChangeBaudrateAction(9600)));
 //   ActionRunner::instance().runAction(ActionPtr(new ComTestAction()));
 //   ActionRunner::instance().runAction(ActionPtr(new ReadStatusAction()));
 //   ActionRunner::instance().runAction(ActionPtr(new ClearAllErrorAction()));
//    ActionRunner::instance().runAction(ActionPtr(new SyncTimeAction()));
    ActionRunner::instance().runAction(ActionPtr(new WriteParameterAction(ParaType::FIR)));
    //ActionRunner::instance().runAction(ActionPtr(new ReadParameterAction()));
    //ActionRunner::instance().runAction(ActionPtr(new StartAcquireAction()));
    //ActionRunner::instance().runAction(ActionPtr(new ReadAcquiredDataAction()));
    //int ramAddress = 0x0000000;
    //int data = 0;
    //int length = 1;
    //ActionRunner::instance().runAction(ActionPtr(new WriteRamDataAction(ramAddress, data, length)));
    //ActionRunner::instance().runAction(ActionPtr(new ReadRamDataAction(ramAddress, length)));
    //ActionRunner::instance().runAction(ActionPtr(new EnterDVPModeAction()));
    //ActionRunner::instance().runAction(ActionPtr(new ExitDVPModeAction()));
    //ActionRunner::instance().runAction(ActionPtr(new TerminateExeAction()));

    for (int i = 0; i < 10; i++)
    {
        int time = 100;

        ActionRunner::instance().update(time);

        BoardStation::instance().update(time);
        Sleep(time);
    }

    BoardStation::instance().close();
}
